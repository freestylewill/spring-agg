package org.springagg.test.casue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springagg.mybatis.Pagination;
import org.springagg.test.base.SpringTestBase;
import org.springagg.web.bean.Student;
import org.springagg.web.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;

public class StudentJUnitTest extends SpringTestBase {
    
    @Autowired
    private StudentService ssv;
    
    @Test
    public void test(){
        Pagination pagination = new Pagination(0, 33);
        List<Student> sts = ssv.all(pagination);
        // 自动查询了总数
        System.out.println(" Total -> " + pagination.getTotal());
        // 打印结果 
        Assert.assertTrue(sts != null && sts.size() > 0);
        for (Student st : sts) {
            System.out.println(" Student -> " + JSON.toJSONString(st));
        }
        
    }
}
